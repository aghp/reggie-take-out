package com.hhxy.config;

import com.hhxy.common.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    /**
     * SpringBoot默认只能访问到static和template目录下的静态资源，通过映射就能访问到此外的静态资源了
     * @param registry
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
        super.addResourceHandlers(registry);
    }

    /**
     * 扩展SpringMVC的消息转换器
     * @param converters
     */
    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 创建一个消息转换器对象
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        // 设置对象转换器（让Jackson将Java对象转成JSON）
        messageConverter.setObjectMapper(new JacksonObjectMapper());
        // 将上面创建的消息转换器对象追加到SpringMVC中的转换器集合中（0表示放到最前面，让SpringMVC优先使用我们自己写的消息转换器）
        converters.add(0, messageConverter);
    }
}
