package com.hhxy.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Configuration;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
@Configuration
public class MyBatisPlusConfig {

    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        // 创建拦截器，并开启MyBatisPlus的分页功能
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

}
