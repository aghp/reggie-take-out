package com.hhxy.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hhxy.common.R;
import com.hhxy.entity.Employee;
import com.hhxy.service.EmployeeService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


    /**
     * 处理员工的登录请求
     *
     * @param employee
     * @param request
     * @return
     */
    @PostMapping("/login")
    public R<Employee> login(@RequestBody Employee employee, HttpServletRequest request) {
        String password = employee.getPassword();

        // 对密码进行md5加密（因为数据库种的密码是通过md5加密的）
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        // 到数据库种查询用户信息，判断是否存在该账号
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        String username = employee.getUsername();
        queryWrapper.eq(Strings.isNotEmpty(username), Employee::getUsername, username);
        // userName字段做了唯一性约束，使用getOne，当查询到多条记录时，直接抛异常
        Employee emp = employeeService.getOne(queryWrapper);

        // 判断当前账号是否存在
        if (emp == null) {
            return R.error("登录失败，用户不存在");
        }
        // 判断密码是否输入正确
        if (!emp.getPassword().equals(password)) {
            return R.error("登录失败，密码错误");
        }
        // 判断账号当前状态
        if (emp.getStatus() == 0) {
            return R.error("登录失败，账号处于禁用状态");
        }

        // 登录成功，将员工的id存入Session中，并返回登录成功的响应信息
        request.getSession().setAttribute("employee", emp.getId());
        return R.success(emp);
    }

    /**
     * 处理员工退出请求
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request){
        // 清理员工登录时存入Session中的id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    /**
     * 新增员工
     * @param employee
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody Employee employee, HttpServletRequest request){
        log.info("员工信息：{}", employee);

        // 设置初始密码，需要进行md5加密处理
        String password = DigestUtils.md5DigestAsHex("123456".getBytes());
        employee.setPassword(password);
        // 设置记录创建时间和更新时间（已使用自动填充）
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
        // 设置记录的创建人和更新人（已使用自动填充）
//        Long id = (Long) request.getSession().getAttribute("employee");
//        employee.setCreateUser(id);
//        employee.setUpdateUser(id);

        // 调用service层的方法，将用户信息存入数据库中
        employeeService.save(employee);
        return R.success("员工添加成功");
    }

    /**
     * 处理员工信息的分页查询请求
     * @param page 当前页码
     * @param pageSize 每页展示的记录条数
     * @param name 查询的员工姓名
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name){
        // 构建分页构造器
        Page pageInfo = new Page(page, pageSize);

        // 构建条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        // 添加过滤条件（使用模糊查询）
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
        // 添加排序条件（更具更新时间降序排序，越晚更新的越在前面显示）
        queryWrapper.orderByDesc(Employee::getUpdateTime);

        // 执行查询，并返回结果
        employeeService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 处理更新状态的请求（只有admin用户才能更新状态）
     * 处理编辑用户信息的请求
     * @param employee
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody Employee employee, HttpServletRequest request){
        // 设置记录的更新时间和更新人（已使用自动填充）
//        Long empId = (Long) request.getSession().getAttribute("employee");
//        employee.setUpdateUser(empId);
//        employee.setUpdateTime(LocalDateTime.now());
        // 更新状态
        employeeService.updateById(employee);
        return R.success("状态修改成功");
    }

    /**
     * 处理数据编辑操作时数据回显的请求（根据id查询员工信息）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id){
        Employee emp = employeeService.getById(id);
        if (emp != null) {
            // 备注：这里并不是员工多余的操作，能够一定程度保障程序更加安全（防止出现数据不同步的bug，比如一个用户编辑，另一个用户删）
            return R.success(emp);
        }
        return R.error("没有查询到对应的员工信息");
    }
}
