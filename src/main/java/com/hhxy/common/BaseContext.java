package com.hhxy.common;

/**
 * @author ghp
 * @date 2023/1/4
 * @title 基于ThreadLocal封装的工具类
 * @description 用于保存和获取用户的id（完善统一填充公共字段）
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 用户设置当前用户的id
     * @param id
     */
    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    public static Long getCurrentId(){
        return threadLocal.get();
    }
}
