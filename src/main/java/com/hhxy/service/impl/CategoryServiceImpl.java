package com.hhxy.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhxy.entity.Category;
import com.hhxy.mapper.CategoryMapper;
import com.hhxy.service.CategoryService;
import org.springframework.stereotype.Service;

/**
 * @author ghp
 * @date 2023/1/5
 * @title
 * @description
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

}
