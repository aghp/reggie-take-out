package com.hhxy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhxy.entity.Employee;
import com.hhxy.mapper.EmployeeMapper;
import com.hhxy.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}
