package com.hhxy.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.entity.Category;

/**
 * @author ghp
 * @date 2023/1/5
 * @title
 * @description
 */
public interface CategoryService extends IService<Category> {
}
