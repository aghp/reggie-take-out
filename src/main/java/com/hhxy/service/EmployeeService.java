package com.hhxy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhxy.entity.Employee;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
public interface EmployeeService extends IService<Employee> {
}
