package com.hhxy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
