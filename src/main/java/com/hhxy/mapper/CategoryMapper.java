package com.hhxy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhxy.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ghp
 * @date 2023/1/5
 * @title
 * @description
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}
