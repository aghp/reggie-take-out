package com.hhxy.filter;

import com.alibaba.fastjson.JSON;
import com.hhxy.common.BaseContext;
import com.hhxy.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ghp
 * @date 2023/1/4
 * @title
 * @description 用于判断用户是否完成登录
 */
@WebFilter(filterName = "loginCheckFilter")
@Slf4j
public class LoginCheckFilter implements Filter {

    // 路径匹配器，支持通配符（用来处理特殊的路径）
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // 获取拦截到请求的uri
        String uri = request.getRequestURI();
        // 设置放行的uri
        String[] uris = new String[]{
          "/employee/login",
          "/employee/logout",
          "/backend/**",
          "/front/**"
        };

        // 判断是否放行本次请求
        boolean check = check(uri, uris);
        if (check) {
            // 放行
            filterChain.doFilter(request, response);
            return;
        }
        // 如果不在放行的uri中，就需要判断当前用户是否已经登录
        Long empId = (Long) request.getSession().getAttribute("employee");
        if (empId != null) {

            // 将存入Session中的当前用户的id存入当前线程中（让MyMetaObjectHandler能够获取到这个id，MyMetaObjectHandler不能使用request对象）
            BaseContext.setCurrentId(empId);

            // 当前用户已登录，放行
            filterChain.doFilter(request, response);
            return;
        }
        // 如果用户不在放行的uri中，且用户当前未登录，则像浏览器响应对应的信息，告诉用户先登录
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;
    }

    /**
     * 检查本次请求是否放行
     * @param uri
     * @return tru表示放行
     */
    public boolean check(String uri, String[] uris){
        for (String u : uris) {
            boolean match = PATH_MATCHER.match(u, uri);
            if (match) {
                // 可以放行
                return true;
            }
        }
        return false;
    }
}
